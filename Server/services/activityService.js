var models  = require('./../models');
var Activity  = require('./../models/activity');

var activityService = {

    add: async function(data) {
        var activity = {
            comment: data.comment,
            type: data.type,
            confirm: false,
            UserId: data.UserId,
        };
        return await models.Activity.create(activity);
    },

    confirm: async function(id) {
        var activity = await this.getById(id);
        if (activity) {
            activity.confirm = true;
        }
        return await activity.save(activity);
    },

    getById: async function(id) {
        return await models.Activity.findOne({ where: {
            id: id
        }});
    },

    delete: async function (id){
        return await models.Activity.destroy({where: {
            id: id,
            confirm: false
        }});
    },

    get: async function(where = {}){
        return await models.Activity.findAll({ where: where });
    }
};

module.exports = activityService;