var models  = require('./../models');

var roleService = {

    get: () => {
        return [
            'Admin', 'Client'
        ];
    },

    check: (name) =>{
        return roleService.get().find(x=>x === name) != null;
    }
};

module.exports = roleService;