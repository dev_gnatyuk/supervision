var cors = require('cors')
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var authentication = require('express-authentication');
var bodyParser  = require('body-parser');

var app = express();

var jwt    = require('jsonwebtoken');
var userService = require('./services/userService');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(async function myauth(req, res, next) {
  // provide the data that was used to authenticate the request; if this is 
  // not set then no attempt to authenticate is registered. 
  //req.challenge = req.get('Authorization');
  var token = req.get('Authorization');
  var user = await userService.getByToken(token);
  req.authenticated = user ? true : false;
  // provide the result of the authentication; generally some kind of user 
  // object on success and some kind of error as to why authentication failed 
  // otherwise. 
  if (req.authenticated) {
      req.authentication = user.dataValues;
  } else {
      req.authentication = { error: 'INVALID_API_KEY' };
  }

  // That's it! You're done! 
  next();
});

app.use('/', require('./controllers/web/index'));
app.use('/api', require('./controllers/api/index'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
