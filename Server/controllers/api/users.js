var router = require('express').Router();
var authentication = require('express-authentication');

var routerPrefix = '/';

var authentication = require('express-authentication');
var rh = require('./../../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

var routerPrefix = '/';

var userService = require('./../../services/userService');


router.post(routerPrefix, [
    check('name').exists().isLength({ max: 256 }),
    check('email').exists().isEmail().trim().normalizeEmail(),
    check('role').exists(),
    check('password').exists().isLength({ max: 64 }),
], /*authentication.required(),*/ async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res,await userService.registration(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix, async function(req, res) {
    res.json(await userService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await userService.getById(req.params.id));
})

router.put(routerPrefix + ":id", [
    check('name').exists().isLength({ max: 256 }),
    check('email').exists().isEmail().trim().normalizeEmail(),
    check('role').exists(),
    check('password').exists().isLength({ max: 64 }),
    authentication.required()
  ], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await userService.edit(req.params.id, req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await userService.delete(req.params.id));
})


module.exports = router