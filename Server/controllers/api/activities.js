var router = require('express').Router();
var authentication = require('express-authentication');

var routerPrefix = '/';

var authentication = require('express-authentication');
var rh = require('./../../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

var routerPrefix = '/';

var userService = require('./../../services/userService');
var activityService = require('./../../services/activityService');


router.post(routerPrefix, [
    check('comment').exists().isLength({ max: 256 }),
    check('type').exists().isLength({ max: 256 }),
    check('UserId').exists(),
], /*authentication.required(),*/ async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res,await activityService.add(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix, async function(req, res) {
    res.json(await activityService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await activityService.getById(req.params.id));
})

router.put(routerPrefix + "confirm/:id", /*authentication.required(),*/ async function(req, res) {
    res.json(await activityService.confirm(req.params.id));
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await activityService.delete(req.params.id));
})


module.exports = router