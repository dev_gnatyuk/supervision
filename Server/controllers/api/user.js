var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

var routerPrefix = "/";

var UserService = require('./../../services/userService');
var RoleService = require('./../../services/roleService');


router.post(routerPrefix + 'login',[
    check('email').exists().isEmail().trim().normalizeEmail(),
    check('password').exists().isLength({ max: 64 }),
], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await UserService.login(req.body.email,req.body.password), {status: 200});
    }catch(e){
        rh.Error(req,res,e);
    }
})

module.exports = router