var router = require('express').Router();

var path = require('path');

router.use('/user',require('./user'));
router.use('/users', require('./users'));
router.use('/activities', require('./activities'));

module.exports = router