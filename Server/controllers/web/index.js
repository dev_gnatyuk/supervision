var router = require('express').Router();

var path = require('path');

router.use('/', require('./home'));
router.use('/users', require('./users'));
/*router.use('/offices',require('./officesController'));
router.use('/users', require('./usersController'));
router.use('/bundles', require('./bundlesController'));
router.use('/flights', require('./flightsController'));
router.use('/transportings', require('./transportingsController'));
router.use('/roles', require('./rolesController'));*/

module.exports = router