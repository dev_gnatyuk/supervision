module.exports = {

    Ok: function(req, res, data, config = {}) {
        if (!config['status']){
            switch(req.method){
                case "POST": {
                    res.status(201);
                    break;
                }
            }
        }

        if (config['status']){
            res.status(config['status']);
        }

        data = JSON.parse(JSON.stringify(data, (k,v) => (k === 'password')? undefined : v))
        
        res.json(data);
    },

    Error: function(req, res, error, code = 500) {
        res.status(code);
        res.json({error: error.message});
    },

};