package com.example.root.supervision.models

import com.google.gson.annotations.SerializedName

class ResAuth {
    @SerializedName("token") var Token: String = ""
    @SerializedName("role") var Role: String = ""
}