package com.example.root.supervision.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.root.supervision.R
import net.glxn.qrgen.android.QRCode
import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView

/*
* val myBitmap = QRCode.from("www.example.org").bitmap()
                val myImage = findViewById<View>(R.id.imageView) as ImageView
                myImage.setImageBitmap(myBitmap)
* */
class QRCodeAdminActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode_admin)

        val userId = this.intent.getStringExtra("Id")

        val myBitmap = QRCode.from(userId).bitmap()
        val myImage = findViewById<View>(R.id.imageView) as ImageView
        myImage.setImageBitmap(myBitmap)

    }
}
