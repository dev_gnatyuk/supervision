package com.example.root.supervision.models

import com.google.gson.annotations.SerializedName

class Activity {
    @SerializedName("id") var Id: Long? = null
    @SerializedName("UserId") var UserId: Long? = null
    @SerializedName("comment") var Comment: String = ""
    @SerializedName("type") var Type: String = ""
    @SerializedName("confirm") var Confirm: Boolean? = null

}