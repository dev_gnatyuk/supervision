package com.example.root.supervision.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.example.root.supervision.R
import com.example.root.supervision.models.Activity
import com.example.root.supervision.services.ActivityService
import kotlinx.android.synthetic.main.activity_edit.*
import net.glxn.qrgen.android.QRCode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        var userId: Long = 1
        var type: String = "Type1"

        create_action.setOnClickListener(View.OnClickListener {
            var activity: Activity = Activity();
            activity.UserId = 1;
            activity.Comment = comment.text.toString();
            activity.Type = type;
            var context = this;
            doAsync{
                var res = ActivityService().create(activity);
                uiThread {
                    val intent = Intent(context, QRCodeAdminActivity::class.java)
                    var id = res.Id
                    intent.putExtra("Id", id.toString())
                    startActivity(intent)
                    context.finish()
                }
            }
        })
    }
}
