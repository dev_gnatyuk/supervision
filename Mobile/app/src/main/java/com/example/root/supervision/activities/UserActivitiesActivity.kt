package com.example.root.supervision.activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import com.example.root.supervision.R
import com.example.root.supervision.lists.ActivitiesList
import com.example.root.supervision.models.Activity
import com.example.root.supervision.models.User
import com.example.root.supervision.services.ActivityService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import android.content.Intent
import kotlinx.android.synthetic.main.app_bar_main.*


class UserActivitiesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_activities)

        val userId = intent.getIntExtra("UserId", 0)

        fab.setOnClickListener { view ->
            val intent = Intent(this, EditActivity::class.java)
            startActivity(intent)
        }

        var activityService = ActivityService()
        var context = this;
        doAsync {
            //Execute all the lon running tasks here
            val activities: List<Activity> = activityService.getAll()
            uiThread {

                var listView = findViewById<ListView>(R.id.user_activities)
                val listItems = ArrayList<Activity>(activities.size)
                for (i in 0 until activities.size) {
                    listItems.add(activities[i])
                }
                val adapter = ActivitiesList(context, listItems)
                listView.adapter = adapter
                listView.setOnItemClickListener { _, _, position, _ ->

                }

            }
        }
    }
}
