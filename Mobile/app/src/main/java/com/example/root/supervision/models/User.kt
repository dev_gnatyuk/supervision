package com.example.root.supervision.models

import com.google.gson.annotations.SerializedName

class User {
    @SerializedName("id") var Id: Long? = null
    @SerializedName("email") var Email: String = ""
    @SerializedName("name") var Name: String = ""
    @SerializedName("role") var Role: String = ""
}