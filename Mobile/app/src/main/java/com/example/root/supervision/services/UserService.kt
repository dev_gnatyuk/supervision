package com.example.root.supervision.services

import com.example.root.supervision.models.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import khttp.get
import kotlin.reflect.KClass

public class UserService {
    fun getAll(): List<User>{
        var usersJson = get("http://78.137.58.66:3000/api/users").text
        val type = object : TypeToken<List<User>>() {}.type
        var users = Gson().fromJson<List<User>>(usersJson, type)
        return users
    }

    fun getById(id: String): User{
        var userJson = get("http://78.137.58.66:3000/api/user/"+id).text
        var user = Gson().fromJson(userJson, User::class.java)
        return user
    }
}