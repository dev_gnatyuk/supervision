package com.example.root.supervision.services

import android.support.annotation.UiThread
import android.support.coreui.R.id.async
import com.example.root.supervision.models.Activity
import com.example.root.supervision.models.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import khttp.get
import khttp.post
import khttp.put
import org.json.JSONObject

class ActivityService {

    fun getAll(): List<Activity>{
        var usersJson = get("http://78.137.58.66:3000/api/activities").text
        val type = object : TypeToken<List<Activity>>() {}.type
        var items = Gson().fromJson<List<Activity>>(usersJson, type)
        return items;
    }

    fun getById(id: String): Activity {
        var userJson = get("http://78.137.58.66:3000/api/activities/"+id).text
        var item = Gson().fromJson(userJson, Activity::class.java)
        return item;
    }

    fun create(activity: Activity): Activity {
        var json = Gson().toJson(activity)
        var userJson = post("http://78.137.58.66:3000/api/activities",json = JSONObject(json) ).text
        var item = Gson().fromJson(userJson, Activity::class.java)
        return item;
    }

    fun confirm(id: String): Activity {
        var userJson = put("http://78.137.58.66:3000/api/activities/confirm/$id").text
        var item = Gson().fromJson(userJson, Activity::class.java)
        return item;
    }


}