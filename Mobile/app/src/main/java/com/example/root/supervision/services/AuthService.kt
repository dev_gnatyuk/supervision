package com.example.root.supervision.services

import com.example.root.supervision.models.Activity
import com.example.root.supervision.models.Auth
import com.example.root.supervision.models.ResAuth
import com.google.gson.Gson
import khttp.post
import org.json.JSONObject

class AuthService {

    companion object {
        var IsAuth: Boolean = false
        var Token: String = ""
        var Role: String = ""
    }

    fun login(auth: Auth): ResAuth {
        var json = Gson().toJson(auth)
        var userJson = post("http://78.137.58.66:3000/api/user/login",json = JSONObject(json) ).text
        var item = Gson().fromJson(userJson, ResAuth::class.java)

        IsAuth = true
        Token = item.Token
        Role = item.Role

        return item;
    }
}